---
# Vars for debops resources role.

# Custom vars.

# Default values.
rdev2__user: "ubuntu"
rdev2__user_home: "/home/{{ rdev2__user }}"

rdev2__docksal_global_vars:
  DOCKSAL_VHOST_PROXY_IP: "0.0.0.0"
  DOCKSAL_VHOST_PROXY_PROXY_PORT_HTTP: "80"
  DOCKSAL_VHOST_PROXY_PROXY_PORT_HTTPS: "443"

# Debops vars.

resources__enabled: true

resources__paths:
  # Correct permissions set by resources__templates operation.
  - path: "{{ rdev2__user_home }}/.aws"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
    recurse: true
  - path: "{{ rdev2__user_home }}/.ssh"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
    recurse: true
  - path: "{{ rdev2__user_home }}/.vim"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
    recurse: true
  # This will only ever happen on the first round, so we don't need to concern
  # ourselves with recursing into child directories (there won't BE any!)
  - path: "{{ rdev2__user_home }}/Dev"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
    recurse: false

  # Ensures ~/.yadm/repo.git exists to work around a debops bug in the users
  # role (which lags yadm a bit on expected paths).
  - path: "{{ rdev2__user_home }}/.yadm"
    src: "{{ rdev2__user_home }}/.config/yadm"
    state: "link"
    force: true

  # Create empty file for z.
  - path: "{{ rdev2__user_home }}/.z"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
    state: "touch"

resources__files: []

resources__commands:
  # Platform.sh-related.
  #
  # Note: script needs no if ... fi because the "creates" property prevents
  # it running when /usr/local/bin/platform exists.
  - name: "Ensure platform.sh cli tool is present"
    shell: |
      # Define variables.
      platform_bin_path="/usr/local/bin/platform"
      release_url_base="https://github.com/platformsh/platformsh-cli/releases"
      release_url=$(curl -I "$release_url_base/latest" | grep location: | cut -d ' ' -f2 | tr -d '\r')

      # Download phar, rename, and make executable.
      curl -L -o /tmp/platform.phar "${release_url_base}/download/${release_url##*/}/platform.phar"
      mv /tmp/platform.phar "$platform_bin_path"
      chmod a+x "$platform_bin_path"
    creates: "/usr/local/bin/platform"
    no_log: false

  - name: "Ensure docksal cli tool is present"
    shell: |
      # Define variables.
      fin_bin_path="/usr/local/bin/fin"
      release_url=$(curl -I "https://github.com/docksal/docksal/releases/latest" | grep location: | cut -d ' ' -f2 | tr -d '\r')

      # Download phar, rename, and make executable.
      curl -L -o "$fin_bin_path" "https://raw.githubusercontent.com/docksal/docksal/${release_url##*/}/bin/fin"
      chmod a+x "$fin_bin_path"
    creates: "/usr/local/bin/fin"
    no_log: false

resources__repositories:
  # Installs nvm.
  - repo: "https://github.com/nvm-sh/nvm"
    dest: "{{ rdev2__user_home }}/.nvm"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"

  # Installs zinit for zsh customization.
  - repo: "https://github.com/zdharma/zinit"
    dest: "{{ rdev2__user_home }}/.zinit/bin"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"

  # Installs Vim plugins.
  - repo: "https://github.com/dense-analysis/ale"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/ale"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/editorconfig/editorconfig-vim"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/editorconfig-vim"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/mattn/emmet-vim"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/emmet-vim"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/junegunn/fzf.vim"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/fzf.vim"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/junegunn/goyo.vim"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/goyo.vim"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/preservim/nerdcommenter"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/nerdcommenter"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/preservim/nerdtree"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/nerdtree"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/vim-vdebug/vdebug"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vdebug"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/ntpeters/vim-better-whitespace"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vim-better-whitespace"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/altercation/vim-colors-solarized"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vim-colors-solarized"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/skammer/vim-css-color"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vim-css-color"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/ryanoasis/vim-devicons"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vim-devicons"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/tpope/vim-commentary"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vim-commentary"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/tpope/vim-fugitive"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vim-fugitive"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/airblade/vim-gitgutter"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vim-gitgutter"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/pangloss/vim-javascript"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vim-javascript"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/sheerun/vim-polyglot"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vim-polyglot"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/styled-components/vim-styled-components"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vim-styled-components"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
  - repo: "https://github.com/tpope/vim-surround"
    dest: "{{ rdev2__user_home }}/.vim/pack/debops/start/vim-surround"
    owner: "{{ rdev2__user }}"
    group: "{{ rdev2__user }}"
