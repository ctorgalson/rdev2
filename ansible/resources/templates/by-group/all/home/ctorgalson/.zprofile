#!/usr/bin/env zsh

# Managed by Ansible. Manual changes will be lost!

# Note: uses zsh syntax!

# Platform.sh-related.
#
# Run platform self:install to set up autocompletion, but don't let it
# update the .zshrc file.
if command -v platform &>/dev/null && [[ ! -d ~/.platformsh ]]
then
  platform self:install --shell-type=zsh --no
fi

# Docksal-related.
#
# Set/update global Docksal-related config vars on every login.
if command -v fin &>/dev/null && [[ -f ~/.docksal/docksal.env ]]
  then
    declare -A finvars

  {% for variable in rdev2__docksal_global_vars %}
    finvars[{{ variable }}]="{{ rdev2__docksal_global_vars[variable] }}"
  {% endfor %}

  for name value in "${(kv)finvars[@]}"
  do
    currentvalue=$(fin config get "$name" --global)

    if [ "$currentvalue" != "\"$value\"" ]
    then
      fin config set "$name=$value" --global
    fi
  done
fi
